ARG PHP_VERSION=7.4
FROM php:${PHP_VERSION}-apache

ARG APACHE_GID=1000
ARG APACHE_UID=1000
ARG COMPOSER_VERSION=2.0.8
ARG MCRYPT_VERSION=1.0.4
ARG REDIS_VERSION=5.3.2
ARG XDEBUG_VERSION=3.0.1

RUN set -ex; \
  usermod -u ${APACHE_UID} www-data; \
  if (( ${APACHE_GID} >= 1000 )); then groupmod -g ${APACHE_GID} www-data; fi; \
  if command -v a2enmod; then \
    a2enmod rewrite; \
    a2enmod headers; \
  fi; \
  savedAptMark="$(apt-mark showmanual)"; \
  apt-get update; \
  apt-get install -y --no-install-recommends \
    libfreetype6-dev \
    libjpeg-dev \
    libpng-dev \
    libpq-dev \
    libonig-dev \
    libxml2-dev \
    libzip-dev; \
  docker-php-ext-configure gd --with-freetype=/usr --with-jpeg=/usr; \
  docker-php-ext-install -j "$(nproc)" \
    calendar \
    gd \
    mbstring \
    opcache \
    pdo_mysql \
    soap \
    sockets \
    zip; \
  pecl install mcrypt-${MCRYPT_VERSION} redis-${REDIS_VERSION} xdebug-${XDEBUG_VERSION} \
    && docker-php-ext-enable igbinary redis; \
  php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"; \
    php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"; \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_VERSION}; \
    php -r "unlink('composer-setup.php');"; \
  apt-mark auto '.*' > /dev/null; \
  apt-mark manual $savedAptMark; \
  ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
    | awk '/=>/ { print $3 }' \
    | sort -u \
    | xargs -r dpkg-query -S \
    | cut -d: -f1 \
    | sort -u \
    | xargs -rt apt-mark manual; \
  apt-get install -y \
    git \
    default-mysql-client \
    unzip; \
  apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
  rm -rf /var/lib/apt/lists/*; \
  { \
    echo 'memory_limit = ${PHP_MEMORY_LIMIT}'; \
    echo 'error_reporting = ${PHP_ERROR_REPORTING}'; \
    echo 'display_errors = ${PHP_DISPLAY_ERRORS}'; \
    echo 'display_startup_errors = ${PHP_DISPLAY_STARTUP_ERRORS}'; \
    echo 'upload_max_filesize = ${PHP_UPLOAD_MAX_FILESIZE}'; \
    echo 'post_max_size = ${PHP_POST_MAX_SIZE}'; \
    echo 'date.timezone = ${PHP_DATE.TIMEZONE}'; \
  } >> /usr/local/etc/php/conf.d/php.ini; \
  { \
    echo 'opcache.enable=${PHP_OPCACHE_ENABLE}'; \
    echo 'opcache.memory_consumption=${PHP_OPCACHE_MEMORY_CONSUMPTION}'; \
    echo 'opcache.interned_strings_buffer=${PHP_OPCACHE_INTERNED_STRINGS_BUFFER}'; \
    echo 'opcache.max_accelerated_files=${PHP_OPCACHE_MAX_ACCELERATED_FILES}'; \
    echo 'opcache.revalidate_freq=${PHP_OPCACHE_REVALIDATE_FREQ}'; \
    echo 'opcache.fast_shutdown=${PHP_OPCACHE_FAST_SHUTDOWN}'; \
  } > /usr/local/etc/php/conf.d/opcache-recommended.ini

WORKDIR /var/www

RUN chown -R www-data.www-data .

ENV PHP_OPCACHE_ENABLE 1
ENV PHP_OPCACHE_MEMORY_CONSUMPTION 128
ENV PHP_OPCACHE_INTERNED_STRINGS_BUFFER 0
ENV PHP_OPCACHE_MAX_ACCELERATED_FILES 4000
ENV PHP_OPCACHE_REVALIDATE_FREQ 60
ENV PHP_OPCACHE_FAST_SHUTDOWN 1
ENV PHP_MEMORY_LIMIT 1024M
ENV PHP_ERROR_REPORTING E_ALL & ~E_DEPRECATED
ENV PHP_DISPLAY_ERRORS 0
ENV PHP_DISPLAY_STARTUP_ERRORS 0
ENV PHP_UPLOAD_MAX_FILESIZE 50M
ENV PHP_POST_MAX_SIZE 50M
ENV PHP_DATE_TIMEZONE 'Europe/Brussels'
