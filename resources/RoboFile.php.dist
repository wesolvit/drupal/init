<?php

namespace {

  use Drupal\Core\Database\Database;
  use Drupal\Core\DrupalKernel;
  use Drupal\Core\Site\Settings;
  use GuzzleHttp\Psr7\Uri;
  use Robo\Robo;
  use Symfony\Component\HttpFoundation\Request;

  /**
   * This is project's console commands configuration for Robo task runner.
   *
   * @see http://robo.li/
   */
  class RoboFile extends \Robo\Tasks {

    use \Boedah\Robo\Task\Drush\loadTasks;

    /**
     * @var array Drupal configuration.
     */
    private $drupalConfig;

    /**
     * @var array Drush configuration.
     */
    private $drushConfig;

    public function __construct() {
      $overrideConfigurationFile = __DIR__ . '/robo.override.yml';
      if (file_exists($overrideConfigurationFile)) {
        Robo::loadConfiguration([$overrideConfigurationFile]);
      }

      $this->drupalConfig = Robo::Config()->get('drupal');
      $this->drushConfig = Robo::Config()->get('drush');
    }

    /**
     * Install the website.
     *
     * @param array $options
     *   Options:
     *     - force: Force installation. (Default: FALSE).
     *
     * @throws \Robo\Exception\TaskException
     */
    public function install($options = ['force' => FALSE]) {
      $continueInstallation = $options['force'] || !$this->isInstalled();
      $continueInstallation = $continueInstallation ?: $this->confirm('Drupal is already installed. Would you continue ? (This will destroy current installation)');

      if (!$continueInstallation) {
        return;
      }

      extract($this->drupalConfig);
      extract($this->drushConfig);
      /** @var $dbhost string */
      /** @var $dbuser string */
      /** @var $dbpass string */
      /** @var $dbname string */
      /** @var $dbport string */
      /** @var $bin string */
      /** @var $root string */
      /** @var $sitemail string */
      /** @var $sitename string */
      /** @var $mail string */
      /** @var $name string */
      /** @var $pass string */
      /** @var $profile string */
      /** @var $language string */

      /** @var Uri $url */
      $url = Uri::fromParts([
        'scheme' => 'mysql',
        'host' => $dbhost,
        'user' => $dbuser,
        'pass' => $dbpass,
        'path' => $dbname,
        'port' => $dbport,
      ]);

      $this
        ->taskDrushStack($bin)
        ->drupalRootDirectory($root)
        ->locale($language)
        ->siteMail($sitemail)
        ->siteName($sitename)
        ->accountMail($mail)
        ->accountName($name)
        ->accountPass($pass)
        ->dbUrl($url)
        ->siteInstall($profile)
        ->disableUpdateStatusModule()
        ->run();
    }

    /**
     * Check if the current drupal is installed.
     *
     * @return bool
     *   True if the current durpal:
     *   - Has connection information
     *   - Boots
     *   - Has a database connection
     *   - Has the system module enabled
     */
    private function isInstalled() {
      $installed = FALSE;
      try {
        $kernel = $this->getDrupalKernel();
        $connectionInfo = Database::getConnectionInfo();
        if ($connectionInfo) {
          $kernel->boot();
          /** @var \Drupal\Core\Database\Driver\mysql\Connection $connection */
          $connection = Drupal\Core\Database\Database::getConnection();
          if ($connection) {
            /** @var \Drupal\Core\Extension\ModuleHandler $moduleHandler */
            $moduleHandler = \Drupal::service('module_handler');
            $installed = $moduleHandler->moduleExists('system');
          }
        }

      }
      catch (Exception $e) {}

      return $installed;
    }

    /**
     * Get drupal kernel.
     *
     * @return \Drupal\Core\DrupalKernel
     */
    private function getDrupalKernel() {
      global $classLoader;
      $request = Request::createFromGlobals();
      $kernel = new DrupalKernel('prod', $classLoader);
      $appRoot = $kernel->getAppRoot();
      DrupalKernel::bootEnvironment($appRoot);
      $sitePath = DrupalKernel::findSitePath($request);
      $kernel->setSitePath($sitePath);
      Settings::initialize($appRoot, $sitePath, $classLoader);
      return $kernel;
    }

  }
}
