<?php

namespace WeSolvIt\DrupalInit;

use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;

/**
 * Class CommandProvider
 *
 * @package WeSolvIt\DrupalInit
 */
class CommandProvider implements CommandProviderCapability {

  /**
   * {@inheritdoc}
   */
  public function getCommands() {
    return [new InitCommand()];
  }

}
