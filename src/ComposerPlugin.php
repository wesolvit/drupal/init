<?php

namespace WeSolvIt\DrupalInit;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\Capable;
use Composer\Plugin\PluginInterface;

/**
 * Class ComposerPlugin
 *
 * @package WeSolvIt\DrupalInit
 */
class ComposerPlugin implements PluginInterface, Capable {

  /**
   * {@inheritdoc}
   */
  public function activate(Composer $composer, IOInterface $io) {
  }

  /**
   * {@inheritdoc}
   */
  public function getCapabilities() {
    return [
      'Composer\\Plugin\\Capability\\CommandProvider' => 'WeSolvIt\\DrupalInit\\CommandProvider',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function deactivate(Composer $composer, IOInterface $io) {
  }

  /**
   * {@inheritdoc}
   */
  public function uninstall(Composer $composer, IOInterface $io) {
  }

}
