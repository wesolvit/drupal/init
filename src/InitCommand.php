<?php

namespace WeSolvIt\DrupalInit;

use Closure;
use Composer\IO\NullIO;
use Composer\Json\JsonFile;
use Exception;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use WeSolvIt\DrupalInit\HussainwebDrupalInitCommand as DrupalInitCommand;

/**
 * Class InitCommand
 *
 * @package WeSolvIt\DrupalInit
 */
class InitCommand extends DrupalInitCommand {

  /**
   * List of environment variables with their values.
   *
   * @var array
   */
  private array $envs = [
    'http_proxy' => '',
    'https_proxy' => '',
    'no_proxy' => 'localhost,127.0.0.1',
  ];

  /**
   * File system.
   *
   * @var \Symfony\Component\Filesystem\Filesystem
   */
  private Filesystem $fileSystem;

  /**
   * Options as array.
   *
   * @var array
   */
  private array $optionsArray;

  /**
   * {@inheritdoc}
   */
  public function __construct(string $name = NULL) {
    parent::__construct($name);
    $this->fileSystem = new Filesystem();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure(): void {
    parent::configure();
    $this->getDefinition()->getOption('web-dir')->setDefault('./');
    $this->getDefinition()->getOption('name')->setDefault('wesolvit/package');
    $this->getDefinition()->getOption('author')->setDefault('Rachid TARSIMI <rachid@wesolv.it>');
    $this->getDefinition()->getOption('stability')->setDefault('dev');
    $this->getDefinition()->getOption('license')->setDefault('GPL-3.0-or-later');
    $this->getDefinition()->getOption('core')->setDefault('drupal/core-recommended ^8.9');
    $this->getDefinition()->addOptions([
      new InputOption('docker', NULL, InputOption::VALUE_OPTIONAL, 'Use docker', TRUE),
      new InputOption('docker-network', NULL, InputOption::VALUE_OPTIONAL, 'Docker network', 'default'),
      new InputOption('docker-image', NULL, InputOption::VALUE_OPTIONAL, 'Docker image', TRUE),
      new InputOption('docker-php-fpm', NULL, InputOption::VALUE_OPTIONAL, 'Docker image with PHP-FPM', TRUE),
      new InputOption('docker-image-php-name', NULL, InputOption::VALUE_OPTIONAL, 'Docker image php name', 'php:7.4-fpm-alpine'),
      new InputOption('docker-image-web-name', NULL, InputOption::VALUE_OPTIONAL, 'Docker image web name', 'nginx:alpine'),
      new InputOption('docker-image-db-name', NULL, InputOption::VALUE_OPTIONAL, 'Docker image database name', 'yobasystems/alpine-mariadb'),
      new InputOption('xdebug-enable', NULL, InputOption::VALUE_OPTIONAL, 'Enable xdebug in php.ini', TRUE),
      new InputOption('xdebug-version', NULL, InputOption::VALUE_OPTIONAL, 'Xdebug version', '3'),
      // @todo Uncomment this when NFS for Docker is handled for macOSX.
      //new InputOption('dev-os', NULL, InputOption::VALUE_OPTIONAL, 'Development operating system (e.g linux, macOSX)', 'linux'),
      new InputOption('app-dir', NULL, InputOption::VALUE_OPTIONAL, 'Application directory', '.'),
      new InputOption('app-port', NULL, InputOption::VALUE_OPTIONAL, 'Application port', '80'),
      new InputOption('app-user-uid', NULL, InputOption::VALUE_OPTIONAL, 'Application user id', '1000'),
      new InputOption('app-user-gid', NULL, InputOption::VALUE_OPTIONAL, 'Application group id', '1000'),
      new InputOption('drupal-uri', NULL, InputOption::VALUE_OPTIONAL, 'Drupal URI', 'http://localhost'),
      new InputOption('drupal-root', NULL, InputOption::VALUE_OPTIONAL, 'Drupal root path', '/var/www/html'),
      new InputOption('drupal-admin-name', NULL, InputOption::VALUE_OPTIONAL, 'Drupal admin name', 'admin'),
      new InputOption('drupal-admin-pass', NULL, InputOption::VALUE_OPTIONAL, 'Drupal admin password', 'pass'),
      new InputOption('drupal-admin-mail', NULL, InputOption::VALUE_OPTIONAL, 'Drupal admin mail', 'mail@mail.com'),
      new InputOption('drupal-profile', NULL, InputOption::VALUE_OPTIONAL, 'Drupal profile', 'standard'),
      new InputOption('drupal-language', NULL, InputOption::VALUE_OPTIONAL, 'Drupal language', 'en'),
      new InputOption('drupal-db-name', NULL, InputOption::VALUE_OPTIONAL, 'Drupal database name', 'drupal'),
      new InputOption('drupal-db-host', NULL, InputOption::VALUE_OPTIONAL, 'Drupal database host', 'db'),
      new InputOption('drupal-db-port', NULL, InputOption::VALUE_OPTIONAL, 'Drupal database port', '3306'),
      new InputOption('drupal-db-user', NULL, InputOption::VALUE_OPTIONAL, 'Drupal database user', 'drupal'),
      new InputOption('drupal-db-pass', NULL, InputOption::VALUE_OPTIONAL, 'Drupal database password', 'drupal'),
      new InputOption('drupal-site-name', NULL, InputOption::VALUE_OPTIONAL, 'Drupal site name', 'Drupal 8'),
      new InputOption('drupal-site-mail', NULL, InputOption::VALUE_OPTIONAL, 'Drupal site mail', 'site@mail.com'),
      new InputOption('drush-bin-path', NULL, InputOption::VALUE_OPTIONAL, 'Drush path', './bin/drush'),
      new InputOption('no-install-dependencies', NULL, InputOption::VALUE_OPTIONAL, 'Install dependencies', TRUE),
    ]);

    $this
      ->setName('wesolvit-init')
      ->setDescription('Create a drupal8 installation with the needed tools.')
      ->setHelp(
        <<<EOT
          The <info>wesolvit-init</info> command creates a composer project 
          in the working directory.
          It generates the composer.json, docker-compose.yml, docker files 
          for Apache-MariaDB-PHP stack, robo.yml, RoboFile.php and provides aliases
          to execute drush, drupal console, composer and robo inside the docker container.
          
          <info>php composer.phar wesolvit-init</info>
        EOT
      );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   *
   * @todo License validator based on identifiers in https://spdx.org/licenses/.
   * @todo Drupal language validator, to allow only language ISO codes.
   */
  protected function interact(InputInterface $input, OutputInterface $output): void {
    parent::interact($input, $output);
    $io = $this->getIO();
    /** @var \Symfony\Component\Console\Helper\FormatterHelper $formatter */
    $formatter = $this->getHelperSet()->get('formatter');
    $io->writeError([
      '',
      $formatter->formatBlock('Wizard for docker and Robo.li', 'bg=blue;fg=white', TRUE),
      '',
    ]);

    $questions = [];
    foreach ($this->getDefinition()->getOptions() as $name => $option) {
      if (preg_match('/^(app|docker|dev|(drupal-[^\d])|drush|xdebug)/', $name) && $name !== 'app-dir') {
        $questions[$name] = [
          'question' => preg_replace('/^(.*?) \(.*\)$/', '$1', $option->getDescription()),
          'default' => $input->getOption($name),
          'is_bool' => is_bool($option->getDefault()),
          'validator' => NULL,
        ];
      }
    }

    $questions['docker-image']['question'] = 'Use a docker image (otherwise a Dockerfile will be provided)';

    $input->setOption('app-dir', '.');
    $dependencies = [
      'docker-network' => 'docker',
      'docker-image' => 'docker',
      'docker-php-fpm' => 'docker-image',
      'docker-image-php-name' => 'docker-image',
      'docker-image-web-name' => 'docker-php-fpm',
      'docker-image-db-name' => 'docker-image',
      'xdebug-enable' => 'docker-image',
      'xdebug-version' => 'docker-image',
    ];

    // @todo Uncomment this when NFS for Docker is handled for macOSX.
    // $questions['dev-os']['validator'] = $this->devOsValidator($input);
    $questions['drupal-uri']['validator'] = $this->drupalUriValidator($input);

    foreach ($questions as $name => $ask) {
      $confirmation = FALSE;
      $defaultDisplay = $ask['is_bool'] ? (($confirmation = $this->isYes($ask['default'])) ? 'yes' : 'no') : $ask['default'];
      $value = $ask['default'];
      $question = $ask['question'] . ' [<comment>' . $defaultDisplay . '</comment>]: ';
      if (isset($dependencies[$name])) {
        if (!$input->getOption($dependencies[$name])) {
          $input->setOption($name, $ask['is_bool'] ? FALSE : '' );
          continue;
        }
      }
      if ($ask['validator']) {
        $value = $io->askAndValidate($question, $ask['validator'], NULL, $value);
      }
      elseif ($ask['is_bool']) {
        $value = $io->askConfirmation($question, $confirmation);
      }
      else {
        $value = $io->ask($question, $value);
      }
      $input->setOption($name, $value);
    }
  }

  /**
   * Get extra required dev packages.
   *
   * @param \Symfony\Component\Console\Input\InputInterface $input
   *   Input.
   *
   * @return array|string[]
   *   List of extra required dev packages.
   */
  protected function getExtraRequireDevs(InputInterface $input): array {
    $requireDevs = array_merge([
      'behat/behat ^3.7',
      'behat/mink ^1.8',
      'behat/mink-goutte-driver ^1.2',
      'digipolisgent/robo-drush  ^4.2',
      'consolidation/robo ^2.0',
      'drupal/coder ^8.3',
      'drupal/console ^1.9',
      'drupal/drupal-extension ^4.1',
      'drush/drush ^10.3',
      'jcalderonzumba/gastonjs ^1.2',
      'jcalderonzumba/mink-phantomjs-driver ^0.3',
      'mikey179/vfsstream ^1.6',
      'phpmd/phpmd ^2.9',
      'phpspec/prophecy ^1.12',
      'phpunit/phpunit ^9.5',
      'squizlabs/php_codesniffer ^3.5',
      'symfony/css-selector ^3.0',
    ]);
    sort($requireDevs);

    return $requireDevs;
  }

  /**
   * Get extra required packages.
   *
   * @param \Symfony\Component\Console\Input\InputInterface $input
   *   Input.
   *
   * @return array|string[]
   *   List of extra required packages.
   */
  protected function getExtraRequires(InputInterface $input): array {
    $extras = [];
    [$corePackage, $version] = explode(' ', $input->getOption('core'));
    if ($corePackage == 'drupal/core-recommended') {
      $extras = array_merge($extras, [
        "drupal/core-composer-scaffold {$version}",
        "drupal/core-project-message {$version}",
        "drupal/core-vendor-hardening {$version}",
      ]);
    }

    $require = array_merge(
      [
        'composer/installers ^1.9',
        'cweagans/composer-patches ^1.7',
        'drupal/config_update ^1.7',
        $input->getOption('core'),
        'drupal/devel ^4.0',
        'drupal/restui ^1.19',
        'drupal/structure_sync ^2.0',
      ], $extras);

    sort($require);
    return $require;
  }

  /**
   * {@inheritdoc}
   *
   * @return int
   *   Status.
   * @throws \Exception
   */
  protected function execute(InputInterface $input, OutputInterface $output): int {
    $envComposerFile = getenv('COMPOSER');

    $appDir = $this->getAppDir($input);

    if (!$this->fileSystem->exists($appDir)) {
      $this->fileSystem->mkdir($appDir, 0755);
    }

    $tempDir = $this->tmpDir();
    $temporaryComposer = $tempDir . '/composer;json';

    $composerFilePath = $appDir . '/composer.json';

    putenv('COMPOSER=' . $temporaryComposer);
    $isInteractive = $input->isInteractive();
    $input->setInteractive(FALSE);

    if (!$isInteractive) {
      $this->repos = $this->getRepositories($input);
    }

    $io = $this->getIO();
    // Set a null IO to avoid output in the console.
    $this->setIO(new NullIO());
    $status = parent::execute($input, new NullOutput());
    $this->setIO($io);
    $input->setInteractive($isInteractive);

    if ($envComposerFile) {
      putenv('COMPOSER=' . $envComposerFile);
    }
    else {
      putenv('COMPOSER');
    }

    if (!$status) {
      $jsonFile = new JsonFile($temporaryComposer);
      $composer = $jsonFile->read();
      unset($composer['extra']['drupal-composer-helper']);
      $composer['extra']['drupal-scaffold'] = [
        'location' => [
          'web-root' => $input->getOption('web-dir'),
        ],
      ];
      $composer['extra']['installer-paths'] = [
        'core' => ["type:drupal-core"],
        'libraries/contrib/{$name}' => ['type:drupal-library'],
        'libraries/custom/{$name}' => ['type:drupal-custom-library'],
        'modules/contrib/{$name}' => ['type:drupal-module'],
        'modules/custom/{$name}' => ['type:drupal-custom-module'],
        'profiles/contrib/{$name}' => ['type:drupal-profile'],
        'profiles/custom/{$name}' => ['type:drupal-custom-profile'],
        'themes/contrib/{$name}' => ['type:drupal-theme'],
        'themes/custom/{$name}' => ['type:drupal-custom-theme'],
        'drush/Commands/contrib/{$name}' => ['type:drupal-drush'],
        'drush/Commands/custom/{$name}' => ['type:drupal-custom-drush'],
      ];
      $composer['extra']['drupal-core-project-message'] = [
        'post-create-project-cmd-message' => explode(PHP_EOL,
          <<<EOT
          <bg=blue;fg=white>                                                         </>
          <bg=blue;fg=white>  Congratulations, you’ve installed the Drupal codebase  </>
          <bg=blue;fg=white>  from the drupal/legacy-project template!               </>
          <bg=blue;fg=white>                                                         </>
          
          <bg=yellow;fg=black>Next steps</>:
            * Install the site: https://www.drupal.org/docs/8/install
            * Read the user guide: https://www.drupal.org/docs/user_guide/en/index.html
            * Get support: https://www.drupal.org/support
            * Get involved with the Drupal community:
                https://www.drupal.org/getting-involved
            * Remove the plugin that prints this message:
                composer remove drupal/core-project-message
          EOT
        ),
      ];

      $this->fileSystem->remove($tempDir);

      $composer['config']['bin-dir'] = 'bin';

      if ($input->isInteractive()) {
        $io->writeError(array('', JsonFile::encode($composer), ''));
        if (!$io->askConfirmation('Do you confirm generation [<comment>yes</comment>]? ', TRUE)) {
          $io->writeError('<error>Command aborted</error>');

          return 1;
        }
      }

      $jsonFile = new JsonFile($composerFilePath);
      $io->writeError('Writing composer.json');
      $jsonFile->write($composer);

      if ($this->isYes($input->getOption('docker'))) {
        $this->initDocker($input);
        $this->initAliases($input);
      }

      $this->initGitignore($input, $appDir);
      $this->initRobo($input, $appDir);
      $this->initEnvs();

      $customGitIgnore = [
        'modules',
        'themes',
        'profiles',
        'drush/Commands'
      ];
      $ignores = [
        '/*',
        '!/custom',
        '!/.gitignore',
      ];
      foreach ($customGitIgnore as $path) {
        $fullPath = $appDir . '/' . $path;
        $fullPathCustom = $fullPath . '/custom';
        if (!$this->fileSystem->exists($fullPathCustom)) {
          $this->fileSystem->mkdir($fullPathCustom, 0755);
        }
        $this->fileSystem->dumpFile($fullPath . '/.gitignore', implode(PHP_EOL, $ignores));
      }

      $installDependencies = !$this->isYes($input->getOption('no-install-dependencies'));
      $default = $installDependencies ? 'yes' : 'no';
      $question = 'Would you like to install dependencies now [<comment>' . $default . '</comment>]? ';
      $hasDependencies = $this->hasDependencies($input->getOptions());
      $install = ($isInteractive && $hasDependencies && $io->askConfirmation($question, $installDependencies)) ||
        (!$isInteractive && $installDependencies && $hasDependencies);
      if ($install) {
        $this->installDependencies($output);
      }
    }

    return $status;
  }

  /**
   * Initialize docker.
   *
   * @param \Symfony\Component\Console\Input\InputInterface $input
   *   Input.
   *
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   * @todo Handle NFS volume for macOSX.
   *
   */
  private function initDocker(InputInterface $input): void {
    $cwd = realpath(".");

    $tokens = [
        'app_dir' => $this->getAppDir($input),
        'web_path' => dirname($input->getOption('drupal-root')),
      ] + $this->optionsToArray($input);

    $dockerComposeFile = $cwd . '/docker-compose.yml';
    $twig = $this->getTwig();
    $this->fileSystem->dumpFile($dockerComposeFile, $twig->render('docker-compose.yml.twig', $tokens));

    $image = $this->isYes($input->getOption('docker-image'));

    $webPath = $cwd . '/docker/web';

    if (!$image) {
      $this->fileSystem->mirror(__DIR__ . '/../resources/docker', $cwd . '/docker');
    }

    if (!$this->fileSystem->exists($webPath)) {
      $this->fileSystem->mkdir($webPath);
    }
    $twig = $this->getTwig();
    $this->fileSystem->dumpFile($webPath . '/php-dev.ini', $twig->render('php-dev.ini.twig', $this->optionsToArray($input)));

    if ($image && $this->isYes($input->getOption('docker-php-fpm'))) {
      $nginxPath = $cwd . '/docker/nginx';
      if (!$this->fileSystem->exists($nginxPath)) {
        $this->fileSystem->mkdir($nginxPath);
      }
      $twig = $this->getTwig();
      $this->fileSystem->dumpFile($nginxPath . '/site.conf', $twig->render('site.conf.twig', $tokens));
    }

    $envsMap = [
      'DOCKER_NETWORK' => 'docker-network',
      'APACHE_UID' => 'app-user-uid',
      'APACHE_GID' => 'app-user-gid',
      'DB_NAME' => 'drupal-db-name',
      'DB_USER' => 'drupal-db-user',
      'DB_PASS' => 'drupal-db-pass',
    ];

    foreach ($envsMap as $name => $option) {
      $this->envs[$name] = $input->getOption($option);
    }
  }

  /**
   * Initialize robo.
   *
   * @param \Symfony\Component\Console\Input\InputInterface $input
   *   Input.
   * @param string $appDir
   *   Application directory.
   *
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  private function initRobo(InputInterface $input, string $appDir): void {
    $roboYml = $appDir . '/robo.yml';

    $twig = $this->getTwig();
    $this->fileSystem->dumpFile($roboYml, $twig->render('robo.yml.twig', $this->optionsToArray($input)));

    if (!$this->fileSystem->exists($appDir . '/RoboFile.php')) {
      $this->fileSystem->copy(__DIR__ . '/../resources/RoboFile.php.dist', $appDir . '/RoboFile.php');
    }
  }

  /**
   *
   */
  private function initGitignore(InputInterface $input, string $appDir): void {
    $gitignore = $appDir . '/.gitignore';

    $twig = $this->getTwig();
    $this->fileSystem->dumpFile($gitignore, $twig->render('gitignore.twig', $this->optionsToArray($input)));
  }

  /**
   * Get application directory normalized.
   *
   * @param \Symfony\Component\Console\Input\InputInterface $input
   *   Input.
   *
   * @return string
   *   Normalized application directory path.
   */
  private function getAppDir(InputInterface $input): string {
    /** @var string $appDir */
    $appDir = $input->getOption('app-dir');
    if (preg_match('#^[^~]#', $appDir) && !preg_match('#\.{1,2}/#', $appDir)) {
      if ($appDir === '.') {
        return '.';
      }
      $appDir = './' . $appDir;
    }

    return $appDir;
  }

//  /**
//   * Development OS validator closure.
//   *
//   * @param \Symfony\Component\Console\Input\InputInterface $input
//   *   Input.
//   *
//   * @return \Closure
//   *   Function to validate the development OS.
//   */
//  private function devOsValidator(InputInterface $input) {
//    return function($value) use ($input) {
//      if (NULL === $value) {
//        return $input->getOption('dev-os');
//      }
//      if (!in_array($value, ['linux', 'macOSX'])) {
//        throw new InvalidArgumentException('The value ' . $value . ' is invalid. Allowed values are linux or macOSX');
//      }
//      return $value;
//    };
//  }

  /**
   * Drupal uri validator closure.
   *
   * @param \Symfony\Component\Console\Input\InputInterface $input
   *   Input.
   *
   * @return \Closure
   *   Function to validate Drupal uri.
   */
  private function drupalUriValidator(InputInterface $input): Closure {
    return function ($value) use ($input) {
      if (NULL === $value) {
        return $input->getOption('drupal-uri');
      }
      $options = NULL;
      if (version_compare(PHP_VERSION, '7.3') < 0) {
        $options = constant('FILTER_FLAG_SCHEME_REQUIRED');
      }

      if (!filter_var($value, FILTER_VALIDATE_URL, $options)
        || (($parts = parse_url($value)) && !in_array($parts['scheme'], ['http', 'https']))
        || preg_match('/[?&#=]/', $value)
      ) {
        throw new Exception(sprintf('The url %s is not valid', $value));
      }

      return $value;
    };
  }

  /**
   * Docker aliases.
   *
   * @param \Symfony\Component\Console\Input\InputInterface $input
   *   Input.
   *
   * @throws \Twig\Error\LoaderError
   * @throws \Twig\Error\RuntimeError
   * @throws \Twig\Error\SyntaxError
   */
  private function initAliases(InputInterface $input): void {
    $drushBin = $input->getOption('drush-bin-path');
    $tokens = $this->optionsToArray($input) + [
        'drupal_bin_path' => dirname($drushBin) . '/drupal',
        'robo_bin_path' => dirname($drushBin) . '/robo',
        'composer_bin_path' => dirname($drushBin) . '/composer',
      ];

    $twig = $this->getTwig();

    $this->fileSystem->dumpFile('.aliases', $twig->render('aliases.twig', $tokens));
  }

  /**
   * Create .env file.
   */
  private function initEnvs(): void {
    $envs = $this->envs;
    array_walk($envs, function (&$value, $key) {
      $value = $key . '=' . $value;
    });

    $this->fileSystem->dumpFile('.env', implode(PHP_EOL, $envs));
  }

  /**
   * Get options as array for twig templates.
   *
   * @param \Symfony\Component\Console\Input\InputInterface $input
   *   Input.
   *
   * @return array
   *   List of options, keyed by name where dashes are replaced by underscores.
   */
  private function optionsToArray(InputInterface $input): array {
    if (empty($this->optionsArray)) {
      foreach ($this->getDefinition()->getOptions() as $name => $option) {
        $value = $input->getOption($name);
        $value = is_bool($option->getDefault()) ? $this->isYes($value) : $value;
        $this->optionsArray[str_replace('-', '_', $name)] = $value;
      }
    }

    return $this->optionsArray;
  }

  /**
   * Get twig.
   *
   * @return \Twig\Environment
   *   Twig environment.
   */
  private function getTwig(): Environment {
    $loader = new FilesystemLoader(__DIR__ . '/../resources/templates');

    return new Environment($loader);
  }

  /**
   * Install dependencies.
   *
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *   Output.
   */
  private function installDependencies(OutputInterface $output): void {
    try {
      $installCommand = $this->getApplication()->find('install');
      $installCommand->run(new ArrayInput([]), $output);
    } catch (Exception $e) {
      $this->getIO()->writeError('Could not install dependencies. Run `composer install` to see more information.');
    }
  }

  /**
   * Check if there are require(-dev) dependencies.
   *
   * @param mixed $options
   *   List of options.
   *
   * @return bool
   *   If has require(-dev) dependencies.
   */
  private function hasDependencies($options): bool {
    $requires = (array)$options['require'];
    $devRequires = isset($options['require-dev']) ? (array)$options['require-dev'] : [];

    return !empty($requires) || !empty($devRequires);
  }

  /**
   * Check if a value is yes.
   *
   * @param mixed $value
   *   Input value.
   *
   * @return bool
   *   Check if the value is yes.
   */
  private function isYes($value): bool {
    return $value === TRUE || $value === 'yes' || $value === 'y' || strtolower($value) === 'true';
  }

  /**
   * Create a temporary directory.
   *
   * @return string
   *   Temporary directory.
   */
  private function tmpDir(): string {
    // Get unique temporary file.
    $tempFile = $this->fileSystem->tempnam(sys_get_temp_dir(), 'wslt');

    if ($this->fileSystem->exists($tempFile)) {
      // Delete the temporary file.
      $this->fileSystem->remove($tempFile);
    }

    // Create a directory with a unique name.
    $this->fileSystem->mkdir($tempFile);

    return $tempFile;
  }

}
