<div style="text-align: center">
  <a href="https://gitlab.com/wesolvit/drupal/init">
    <img src="https://assets.gitlab-static.net/uploads/-/system/group/avatar/9872690/logo.png?width=80" alt="Logo" width="80" height="80" />
  </a>

  <h3>WeSolvIT Drupal init</h3>

  <p>
    Create a Drupal project using composer.
  </p>
</div>

## Table of Contents

* [About this project](#about-this-project)
* [Getting Started](#getting-started)
* [Usage](#usage)
  * [Docker support](#docker-support)
* [Future](#future)
* [License](#license)
* [Contact](#contact)

## About this project

Many will say, "Yet another create Drupal project", and I will say yes.
But this one provides Docker support.

It will create your Drupal (8|9) project and with a docker-compose.yml.

It can be used interactively or not (especially for CI).

This plugin generates a composer.json for your project, a docker-compose.yml if you want to use docker and RoboFile.php and robo.yml for your automation.

The first version was depending on [hussainweb/drupal-composer-init](https://github.com/hussainweb/drupal-composer-init), but not anymore as it is not compatible with composer 2.

I also dropped the Drupal 7 support and php lesser than 7.4.

## Getting Started

```shell
composer global require wesolvit/drupal-init

mkdir my-new-project
cd my-new-project
composer wesolvit-init
```

## Usage

The tool will ask you several questions about your project. You can provide some default values via the command line.\
Here are the options:
```help
Usage:
  wesolvit-init [options]

Options:
      --name=NAME                                          Name of the package [default: "wesolvit/package"]
      --description=DESCRIPTION                            Description of package
      --author=AUTHOR                                      Author name of package [default: "Rachid TARSIMI <rachid@wesolv.it>"]
      --type[=TYPE]                                        Type of package (e.g. library, project, metapackage, composer-plugin) [default: "project"]
      --homepage=HOMEPAGE                                  Homepage of package
      --require=REQUIRE                                    Package to require with a version constraint, e.g. foo/bar:1.0.0 or foo/bar=1.0.0 or "foo/bar 1.0.0" (multiple values allowed)
      --require-dev=REQUIRE-DEV                            Package to require for development with a version constraint, e.g. foo/bar:1.0.0 or foo/bar=1.0.0 or "foo/bar 1.0.0" (multiple values allowed)
  -c, --core=CORE                                          Drupal Core or distribution, e.g. drupal/core or acquia/lightning [default: "drupal/core-recommended ^8.9"]
  -s, --stability=STABILITY                                Minimum stability (empty or one of: stable, RC, beta, alpha, dev) [default: "dev"]
  -l, --license=LICENSE                                    License of package [default: "GPL-3.0-or-later"]
      --repository=REPOSITORY                              Add custom repositories, either by URL or using JSON arrays (multiple values allowed)
  -w, --web-dir=WEB-DIR                                    Specify the docroot (defaults to web) [default: "./"]
      --docker[=DOCKER]                                    Use docker [default: true]
      --docker-network[=DOCKER-NETWORK]                    Docker network [default: "default"]
      --docker-image[=DOCKER-IMAGE]                        Docker image [default: true]
      --docker-php-fpm[=DOCKER-PHP-FPM]                    Docker image with PHP-FPM [default: true]
      --docker-image-php-name[=DOCKER-IMAGE-PHP-NAME]      Docker image php name [default: "php:7.4-fpm-alpine"]
      --docker-image-web-name[=DOCKER-IMAGE-WEB-NAME]      Docker image web name [default: "nginx:alpine"]
      --docker-image-db-name[=DOCKER-IMAGE-DB-NAME]        Docker image database name [default: "yobasystems/alpine-mariadb"]
      --app-dir[=APP-DIR]                                  Application directory [default: "."]
      --app-port[=APP-PORT]                                Application port [default: "80"]
      --app-user-uid[=APP-USER-UID]                        Application user id [default: "1000"]
      --app-user-gid[=APP-USER-GID]                        Application group id [default: "1000"]
      --drupal-uri[=DRUPAL-URI]                            Drupal URI [default: "http://localhost"]
      --drupal-root[=DRUPAL-ROOT]                          Drupal root path [default: "/var/www/html"]
      --drupal-admin-name[=DRUPAL-ADMIN-NAME]              Drupal admin name [default: "admin"]
      --drupal-admin-pass[=DRUPAL-ADMIN-PASS]              Drupal admin password [default: "pass"]
      --drupal-admin-mail[=DRUPAL-ADMIN-MAIL]              Drupal admin mail [default: "mail@mail.com"]
      --drupal-profile[=DRUPAL-PROFILE]                    Drupal profile [default: "standard"]
      --drupal-language[=DRUPAL-LANGUAGE]                  Drupal language [default: "en"]
      --drupal-db-name[=DRUPAL-DB-NAME]                    Drupal database name [default: "drupal"]
      --drupal-db-host[=DRUPAL-DB-HOST]                    Drupal database host [default: "db"]
      --drupal-db-port[=DRUPAL-DB-PORT]                    Drupal database port [default: "3306"]
      --drupal-db-user[=DRUPAL-DB-USER]                    Drupal database user [default: "drupal"]
      --drupal-db-pass[=DRUPAL-DB-PASS]                    Drupal database password [default: "drupal"]
      --drupal-site-name[=DRUPAL-SITE-NAME]                Drupal site name [default: "Drupal 8"]
      --drupal-site-mail[=DRUPAL-SITE-MAIL]                Drupal site mail [default: "site@mail.com"]
      --drush-bin-path[=DRUSH-BIN-PATH]                    Drush path [default: "./bin/drush"]
      --no-install-dependencies[=NO-INSTALL-DEPENDENCIES]  Install dependencies [default: true]
  -h, --help                                               Display this help message
  -q, --quiet                                              Do not output any message
  -V, --version                                            Display this application version
      --ansi                                               Force ANSI output
      --no-ansi                                            Disable ANSI output
  -n, --no-interaction                                     Do not ask any interactive question
      --profile                                            Display timing and memory usage information
      --no-plugins                                         Whether to disable plugins.
  -d, --working-dir=WORKING-DIR                            If specified, use the given directory as working directory.
      --no-cache                                           Prevent use of the cache
  -v|vv|vvv, --verbose                                     Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Help:
    The wesolvit-init command creates a composer project
    in the working directory.
    It generates the composer.json, docker-compose.yml, docker files
    for Apache-MariaDB-PHP stack, robo.yml, RoboFile.php and provides aliases
    to execute drush, drupal console, composer and robo inside the docker container.

    php composer.phar wesolvit-init

```

Examples:
```
# In each new terminal:
. .aliases

# Instead of
docker-compose exec -u www-data ./bin/robo install
# You can do
robo install

# Instead of
docker-compose exec -u www-data drupal ./bin/drush --root=/var/www/html -l http://localhost cr
# You can do
drush cr
```

List of files generated with `composer wesolvit-init --no-interactive`:
```
├── .aliases
├── composer.json
├── docker
│   ├── nginx
│   │   └── site.conf
│   └── web
│       └── php-dev.ini
├── docker-compose.yml
├── drush
│   └── Commands
│       ├── custom
│       └── .gitignore
├── .env
├── .gitignore
├── modules
│   ├── custom
│   └── .gitignore
├── profiles
│   ├── custom
│   └── .gitignore
├── RoboFile.php
├── robo.yml
└── themes
    ├── custom
    └── .gitignore

```

### Docker support ###
There are 2 ways to support docker, either with an image or with a Dockerfile.

Two Dockerfile are provided, one for php using apache and the other for the database using mariadb.

When using an image, you have the choice to use php-fpm where you will need one image for the php and one for nginx.

Example with docker:
```sh
# No FPM
composer wesolvit-init --docker-php-fpm=no
```

```
# No image, but with the provided Dockerfiles
composer weesolvit-init --docker-image=no
```

```
# FPM with a different fpm image.
composer weesolvit-init --docker-image-php-name=my.private.registry/my-php-image:my-tag
```

```
# CI, generate files and install project
composer weesolvit-init --no-interaction --drupal-db-name=${JENKINS_DB_PWD} --no-install-dependencies=no
. .aliases
docker-compose up -d
robo install
```

```
#No docker
composer wesolvit-init --no-interaction --docker=no --no-install-dependencies=no
./bin/robo install
```

**Feel free to adapt the docker-compose, Dockerfiles and the aliases files to your usage.**

## Future

What's coming next

* Support of [traefik](https://traefik.io/)
* Testing
* Custom Dockerfiles
* Alternative choices for PHP-FPM load balancer. (Ex. Apache)

## License

Distributed under the *GNU GENERAL PUBLIC LICENSE Version 3*, License. See `LICENSE` for more information.

## Contact

Rachid TARSIMI - [![LinkedIn][linkedin-shield]][linkedin-url] - rachid@wesolv.it

Project Link: [https://gitlab.com/wesolvit/drupal/init](https://gitlab.com/wesolvit/drupal/init)

<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/rachid-tarsimi
